class Person {
  constructor(name, age, salary, sex) {
    this.name = name;
    this.age = age;
    this.salary = salary;
    this.sex = sex;
  }

  static qs(arr, field, order) { 
    //console.log(arr);
    if (arr.length <= 1) {
      return arr;
    }

    const pivot = arr[arr.length - 1][field];  //getting each value from instances

    const leftArr = [];
    const rightArr = [];

    for (const el of arr.slice(0, arr.length - 1)) {  
      //console.log(el);
      if(order === "asc"){
        el[field] < pivot ? leftArr.push(el) : rightArr.push(el);
      }else if(order === "dsc"){
        el[field] > pivot ? leftArr.push(el) : rightArr.push(el); 
      }
    }

    //using recursion
    return [
      ...this.qs(leftArr, field, order),
      arr[arr.length - 1],
      ...this.qs(rightArr, field, order),
    ];
  }
}

const person1 = new Person("amit", 21, 14000, "male");
const person2 = new Person("sumit", 22, 3500, "male");
const person3 = new Person("suman", 34, 25000, "female");
const person4 = new Person("mahesh", 44, 12000, "male");
const person5 = new Person("rekha", 29, 12500, "female");
const person6 = new Person("anuj", 25, 32100, "male");
const person7 = new Person("krutika", 46, 41000, "female");


const arrPersons = [person1, person2, person3, person4, person5, person6, person7];

//Sorting by age in ascending order
console.log("Array before sorting:");
console.log(arrPersons);
console.log("Array sorting by field age and order is ascending:")
const ans = Person.qs(arrPersons, "age", "asc");
console.log(ans);

//sorting by salary in descending order
console.log("Array before sorting:");
console.log(arrPersons);
console.log("Array sorting by field salary and order is descending:")
const ans2 = Person.qs(arrPersons, "salary", "dsc");
console.log(ans2);